#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/unistd.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include "HD44780.h"

#define DEVICE_NAME "hd44780_i2c"
#define CLASS_NAME  "lcd"
#define MAX_MSG_LEN	33

static int i2c_bus = 0;
static int dev_open(struct inode *inodep, struct file *filep);
static ssize_t dev_write (struct file *file, const char __user *buf, 
	size_t size, loff_t * ppos);
static ssize_t dev_read(struct file *filp, char *buffer, 
	size_t length, loff_t *offset);
static int dev_release(struct inode *inode, struct file *file);

static struct class*  ebbcharClass  = NULL;
static struct device* ebbcharDevice = NULL;
static int majorNumber;
static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = dev_open,
	.write = dev_write,
	.read = dev_read,
	.release = dev_release,
};
static struct i2c_adapter* i2c_dev = NULL;
static struct i2c_client* i2c_client = NULL;

static void i2c_wr(uint8_t value) 
{
	i2c_smbus_write_byte(i2c_client, value);
}

static void delay_us (uint16_t us)
{
	udelay(us);
}

static HD44780_Connection_TypeDef conn;
static HD44780_StructTypeDef lcd = { 
	.WriteData = i2c_wr, 
	.Delay_Func = delay_us,
	.HD44780_Connection = &conn ,
	.Font = ENGLISH_RUSSIAN_FONT,
	.LCD_Type = LCD_OLED,
	.HD44780_ExtConnection = NULL,
};

static int __init dev_init(void)
{
	struct i2c_board_info i2c_info;
	/* Try connect using different address */
	const unsigned short pcf8574[] = 
		{ 0x27, 0x26, 0x25, 0x24, 0x23, 0x22, 0x21, 0x20, I2C_CLIENT_END };
	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
	ebbcharClass = class_create(THIS_MODULE, CLASS_NAME);
	ebbcharDevice = device_create(ebbcharClass, NULL, MKDEV(majorNumber, 0), 
		NULL, DEVICE_NAME);
	
	i2c_dev = i2c_get_adapter(i2c_bus);
	if (!i2c_dev) {
		printk(KERN_ERR DEVICE_NAME " adapter %i not found\n", i2c_bus);
		return EBUSY;
	}
	
	memset(&i2c_info, 0, sizeof(struct i2c_board_info));
	strcpy(i2c_info.type, "PCF8574");
	i2c_client = i2c_new_probed_device(i2c_dev, &i2c_info, pcf8574, NULL);
	if (!i2c_client) {
		printk(KERN_ERR DEVICE_NAME " client failed\n");
		i2c_put_adapter(i2c_dev);
		return EBUSY;
	}
	
	HD44780_StdConnectionInit(&conn, PCF8574_eBayPCB);
	HD44780_Init(&lcd);
	HD44780_Clear();
	HD44780_Print("     READY");

	printk(KERN_INFO DEVICE_NAME " registered\n");
	return 0;
}

static void __exit dev_exit(void)
{
	i2c_unregister_device(i2c_client);
	i2c_put_adapter(i2c_dev);

	i2c_dev = NULL;
	i2c_client = NULL;

	device_destroy(ebbcharClass, MKDEV(majorNumber, 0));
	class_unregister(ebbcharClass);
	class_destroy(ebbcharClass);
	unregister_chrdev(majorNumber, DEVICE_NAME);

	printk(KERN_INFO DEVICE_NAME " unregistered\n");
}

static int dev_open(struct inode *inodep, struct file *filep)
{
	printk(KERN_INFO DEVICE_NAME " open not supported\n");
	
	return 0;
}

static ssize_t dev_write (struct file *file, const char __user *buf, 
	size_t size, loff_t * ppos)
{
	static char message[MAX_MSG_LEN];
	if (size > MAX_MSG_LEN) {
		printk(KERN_ERR DEVICE_NAME " message too long\n");
		return EINVAL;
	}
	
	if (copy_from_user(message, buf, size)) {
		printk(KERN_ERR DEVICE_NAME " %u bytes copy error\n", size);
		return EINVAL;
	}

	HD44780_Clear();
	HD44780_Print(message);
	printk(KERN_INFO DEVICE_NAME " %s\n", message);
	
	return size;
}

static ssize_t dev_read(struct file *filp, char *buffer, 
	size_t length, loff_t *offset)
{
	printk(KERN_INFO DEVICE_NAME " reading not supported\n");
	
	return 0;
}

static int dev_release(struct inode *inode, struct file *file)
{
	printk(KERN_INFO DEVICE_NAME " releasing not supported\n");
	
	return 0;
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pavel Nadein");
MODULE_DESCRIPTION("HD44780_PCF8574_I2C");
MODULE_VERSION("0.1");
MODULE_INFO(intree, "Y");

module_init(dev_init);
module_exit(dev_exit);
