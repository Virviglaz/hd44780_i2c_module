exec = hd44780_i2c
obj-m += $(exec).o
$(exec)-objs := HD44780.o hd44780_module.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
install:
	sudo insmod $(exec).ko
	sudo chmod 666 /dev/$(exec)
remove:
	sudo rmmod $(exec).ko
info:
	modinfo $(exec).ko